import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecipesStoreService } from '../recipes-store.service';

@Component({
  selector: 'app-edit-ingredient-dialog',
  templateUrl: './edit-ingredient-dialog.component.html',
  styleUrls: ['./edit-ingredient-dialog.component.scss']
})
export class EditIngredientDialogComponent implements OnInit {
  formData:any = {};

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private recipesStore: RecipesStoreService,
    public dialogRef: MatDialogRef<EditIngredientDialogComponent>
  ) { }

  ngOnInit(): void {
    this.formData = {
      ingrediantId: 0,
      ingrediantName: '',
      quantity: '',
      ...this.data
    };
  }

  onSubmit() {
    this.recipesStore.editIngredients(
      this.formData.ingrediantId,
      this.formData.ingrediantName,
      this.formData.quantity,
      this.formData.recipeId
    ).subscribe((data:any) => {
      alert(data.message);
      this.onClose();
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
