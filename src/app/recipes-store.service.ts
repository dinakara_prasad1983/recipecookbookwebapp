import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class RecipesStoreService {
  constructor(private http: HttpClient) { }

  getRecipes() {
    return this.http.get('https://localhost:5001/api/RecipeCookBook/AllRecipes');
  }

  deleteRecipe(recipeId: string) {
      return this.http.get(`https://localhost:5001/api/RecipeCookBook/RemoveRecipe?recipeId=${recipeId}`);
  }

  deleteIngredient(ingredientId: string) {
    return this.http.get(`https://localhost:5001/api/RecipeCookBook/RemoveIngredaint?ingredaintId=${ingredientId}`);
  }

  editRecipe(recipeId: string, recipeName: string, description: string): Observable<any> {
      const headers = new HttpHeaders()
        .set('content-type', 'application/json');
      
      return this.http.post('https://localhost:5001/api/RecipeCookBook/Recipe', {
        recipeId: recipeId,
        recipeName: recipeName,
        desription: description
      },
      {headers}
      );
  }

  editIngredients(ingredientId: string, ingredientName: string, quantity: string, recipeId: string) {
      return this.http.post('https://localhost:5001/api/RecipeCookBook/Ingredaint', {
        ingrediantId: ingredientId,
        ingrediantName: ingredientName,
        quantity: quantity,
        recipeId: recipeId
      });
  }
}