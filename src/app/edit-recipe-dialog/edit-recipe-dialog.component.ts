import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecipesStoreService } from '../recipes-store.service';

@Component({
  selector: 'app-edit-recipe-dialog',
  templateUrl: './edit-recipe-dialog.component.html',
  styleUrls: ['./edit-recipe-dialog.component.scss']
})
export class EditRecipeDialogComponent implements OnInit {
  formData:any = {};

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private recipesStore: RecipesStoreService,
    public dialogRef: MatDialogRef<EditRecipeDialogComponent>
  ) { }

  ngOnInit(): void {
    this.formData = {
      recipeName: '',
      recipeId: 0,
      desription: '',
      ...this.data
    };
  }

  onSubmit() {
    this.recipesStore.editRecipe(
      this.formData.recipeId,
      this.formData.recipeName,
      this.formData.desription
    ).subscribe((data:any) => {
      alert(data.message);
      this.onClose();
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
