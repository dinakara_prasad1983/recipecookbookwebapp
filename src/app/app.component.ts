import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditIngredientDialogComponent } from './edit-ingredient-dialog/edit-ingredient-dialog.component';
import { EditRecipeDialogComponent } from './edit-recipe-dialog/edit-recipe-dialog.component';
import { RecipesStoreService } from './recipes-store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'recipes-store';
  panelOpenState=false;
  recipes:any = [];

  constructor(
    private recipesStore: RecipesStoreService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.getRecipes();
  }

  editRecipe(recipe:any, isEditMode: boolean) {
    const dialogRef = this.dialog.open(EditRecipeDialogComponent, {
      data: {
        ...recipe,
        isEditMode: isEditMode
      }
    });

    dialogRef.afterClosed().subscribe(result => {
        this.getRecipes();
    });
  }

  editIngredients(item:any,recipeId: string, isEditMode: boolean) {
    const dialogRef = this.dialog.open(EditIngredientDialogComponent, {
      data: {
        ...item,
        recipeId: recipeId,
        isEditMode: isEditMode
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getRecipes();
  });
  }

  delRecipe(recipe:any) {
    this.recipesStore.deleteRecipe(recipe.recipeId).subscribe((data: any) => {
      alert(data.message);
      this.getRecipes();
    });
  }

  delIngredient(recipe:any) {
    this.recipesStore.deleteIngredient(recipe.ingrediantId).subscribe((data: any) => {
      alert(data.message);
      this.getRecipes()
    });
  }

  getRecipes() {
    this.recipesStore.getRecipes().subscribe((data:any) => {
      console.log('data', data);
      this.recipes = [...data];
    });
  }
}
